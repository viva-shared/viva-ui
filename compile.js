//@ts-check
const lib_fs = require('fs-extra')
const lib_path = require('path')
const lib_compile = require('./site_static_compiler/compile.js')

let store = lib_compile.go(lib_path.join(__dirname, 'site_source'), 'vue/test-component.vue', 'test-component')
let site_path = lib_path.join(__dirname, 'site_compiled')

lib_fs.emptyDirSync(site_path)
lib_fs.writeFileSync(lib_path.join(site_path, 'store.js'), store, {encoding: 'utf8'})
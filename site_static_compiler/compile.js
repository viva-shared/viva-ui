//@ts-check

const lib_fs = require('fs-extra')
const lib_path = require('path')
const lib_os = require('os')
const lib_conv = require('viva-convert')
const to_store_from_css = require('./to_store_from_css').go
const to_store_from_js = require('./to_store_from_js').go
const to_store_from_vue = require('./to_store_from_vue').go
const to_store_from_html = require('./to_store_from_html').go
const to_store_from_scss = require('./to_store_from_scss').go
const to_store_from_map = require('./to_store_from_map').go

exports.go = go
exports.file_data_beautify = file_data_beautify

/**
 * @typedef type_files_for_store
 * @property {string} full_file_name
 * @property {string} relative_name
 * @property {string} ext
 */

/**
 * @typedef type_to_store
 * @property {string} data_base64
 * @property {string} content_type
 */

/**
 * @param {string} source_path
 * @param {string} [root_vue_file_path]
 * @param {string} [root_vue_name]
 * @returns {string}
 */
function go (source_path, root_vue_file_path, root_vue_name) {
    lib_fs.ensureDirSync(source_path)
    source_path = lib_conv.replaceAll(source_path, '\\', '/')

    let file_store_data = [
        "//@ts-check",
        "const os = require('os')",
        "/**",
        " * @typedef type_store",
        " * @property {string} path",
        " * @property {string} content_type",
        " * @property {string} data_base64",
        " */",
        "/**",
        " * @typedef type_store_get",
        " * @property {string} path",
        " * @property {string} content_type",
        " * @property {string} data",
        " */",
        "/** @type {type_store[]} */",
        "let list = []",
        "exports.list = list",
        "exports.get = get",
        "/**",
        " * @param {string} path",
        " * @returns {type_store_get}",
        " */",
        "function get(path) {",
        "    let fnd = list.find(f => typeof path === 'string' && typeof f.path === 'string' && path.toLowerCase() === f.path.toLowerCase())",
        "    if (typeof fnd === 'object' && typeof fnd.data_base64 === 'string') {",
        "        return {",
        "            path: fnd.path,",
        "            content_type: fnd.content_type,",
        "            data: Buffer.from(fnd.data_base64, 'base64').toString('utf8')",
        "        }",
        "    }",
        "    return undefined",
        "}"
    ]

    let errors = 0

    /** @type {type_files_for_store[]} */
    let files_for_store = walk(source_path).map(m => { 
        return {
            full_file_name: lib_conv.replaceAll(m, '\\', '/'),
            relative_name: lib_conv.replaceAll(m, '\\', '/').substring(source_path.length, m.length),
            ext: lib_path.extname(m)
        }
    })

    files_for_store.forEach(file => {
        file_store_data.push("")

        if (lib_conv.equal(file.ext, '.html')) {
            let to_store = to_store_from_html(lib_fs.readFileSync(file.full_file_name, 'utf8'))
            let path = file.relative_name
            let data = to_store.data_base64
            if (lib_conv.equal(file.relative_name, '/index.html')) {
                path = '/'
                data = Buffer.from(to_store.data_base64, 'base64').toString('utf8')
                if (!lib_conv.isEmpty(root_vue_file_path)) {
                    data = lib_conv.replaceAll(data, "<!-- <script src='{viva-ui-root-component_path}'></script> -->", lib_conv.format("<script src='{0}'></script>", lib_conv.toString(root_vue_file_path, '')))
                }
                if (!lib_conv.isEmpty(root_vue_name)) {
                    data = lib_conv.replaceAll(data, "<!-- {viva-ui-root-component} -->", lib_conv.format("<{0}></{0}>", lib_conv.toString(root_vue_name, '')))
                }
                data = file_data_beautify(data)
            }
            file_store_data.push(lib_conv.format("list.push({path: '{0}', content_type: '{1}', data_base64: '{2}'})", [path, to_store.content_type, data]))
            return
        }

        if (lib_conv.equal(file.ext, '.css')) {
            let to_store = to_store_from_css(lib_fs.readFileSync(file.full_file_name, 'utf8'))
            file_store_data.push(lib_conv.format("list.push({path: '{0}', content_type: '{1}', data_base64: '{2}'})", [file.relative_name, to_store.content_type, to_store.data_base64]))
            return
        }

        if (lib_conv.equal(file.ext, '.scss')) {
            let to_store = to_store_from_scss(lib_fs.readFileSync(file.full_file_name, 'utf8'))
            file_store_data.push(lib_conv.format("list.push({path: '{0}', content_type: '{1}', data_base64: '{2}'})", [file.relative_name, to_store.content_type, to_store.data_base64]))
            return
        }

        if (lib_conv.equal(file.ext, '.map')) {
            let to_store = to_store_from_map(lib_fs.readFileSync(file.full_file_name, 'utf8'))
            file_store_data.push(lib_conv.format("list.push({path: '{0}', content_type: '{1}', data_base64: '{2}'})", [file.relative_name, to_store.content_type, to_store.data_base64]))
            return
        }

        if (lib_conv.equal(file.ext, '.js')) {
            let to_store = to_store_from_js(lib_fs.readFileSync(file.full_file_name, 'utf8'))
            file_store_data.push(lib_conv.format("list.push({path: '{0}', content_type: '{1}', data_base64: '{2}'})", [file.relative_name, to_store.content_type, to_store.data_base64]))
            return
        }

        if (lib_conv.equal(file.ext, '.vue')) {
            let component_name = lib_path.basename(file.relative_name).substring(0, lib_path.basename(file.relative_name).length - lib_path.extname(file.relative_name).length)
            let to_store = to_store_from_vue(lib_fs.readFileSync(file.full_file_name, 'utf8'), component_name)
            file_store_data.push(lib_conv.format("list.push({path: '{0}', content_type: '{1}', data_base64: '{2}'})", [file.relative_name, to_store.content_type, to_store.data_base64]))
            return
        }

        errors++
        console.error("unknown file ".concat(file.relative_name))
    })

    if (errors <= 0) {
        console.log('BUID DONE')
    } else {
        console.error('BUID DONE WITH ERRORS '.concat(errors.toString()))
    }

    return file_store_data.join(lib_os.EOL)
}

/** @returns {string[]} */
function walk(dir) {
    var results = []
    var list = lib_fs.readdirSync(dir)
    list.forEach(function(file) {
        file = dir + '/' + file
        var stat = lib_fs.statSync(file)
        if (stat && stat.isDirectory()) { 
            results = results.concat(walk(file))
        } else { 
            results.push(file)
        }
    });
    return results
}

/**
 * @param {string} file_data
 * @returns {string}
 */
function file_data_beautify(file_data) {
    return Buffer.from(file_data,'utf8').toString('base64')
    // let ret = lib_conv.replaceAll( lib_conv.toString(file_data, ''),"'","\\'")
    // ret = lib_conv.replaceAll(ret, lib_os.EOL, "{viva-ui-enter}")
    // ret = lib_conv.replaceAll(ret, "\n", "{viva-ui-enter}")
    // return ret
}

// /**
//  * @property {string} vue
//  * @property {string} name
//  * @returns {string}
//  */
// function vue_to_js(vue, name) {
//     let v = lib_conv.toString(vue, '').replace(/<\!--.*?-->/g, "")

//     let find_template_start = v.indexOf('<template>')
//     if (find_template_start < 0) {
//         throw Error('in component not find "find_template_start"')
//     }
//     let find_template_stop = v.indexOf('</template>', find_template_start)
//     if (find_template_stop < 0) {
//         throw Error('in component not find "find_template_stop"')
//     }
//     let template = v.substring(find_template_start, find_template_stop + '</template>'.length)

//     let find_script_start = v.indexOf('<script>')
//     if (find_script_start < 0) {
//         throw Error('in component not find "find_script_start"')
//     }
//     let find_script_stop = v.indexOf('</script>', find_script_start)
//     if (find_script_stop < 0) {
//         throw Error('in component not find "find_script_stop"')
//     }
//     let script = lib_strip(v.substring(find_script_start + '<script>'.length, find_script_stop), undefined).trim()

//     if (script.substring(0, 'export'.length) !== 'export') {
//         throw Error('in component not find "export default {" in start script')
//     } else {
//         script = script.substring('export'.length, script.length).trim()
//     }
//     if (script.substring(0, 'default'.length) !== 'default') {
//         throw Error('in component not find "export default {" in start script')
//     } else {
//         script = script.substring('default'.length, script.length).trim()
//     }
//     if (script.substring(0, '{'.length) !== '{') {
//         throw Error('in component not find "export default {" in start script')
//     } else {
//         script = script.substring('{'.length, script.length).trim()
//     }
//     if (script.substring(script.length - '}'.length, script.length) !== '}') {
//         throw Error('in component not find "}" in end script')
//     } else {
//         script = script.substring(0, script.length - '}'.length).trim()
//     }

//     let script_part_list = []

//     // @ts-ignore
//     lib_esprima.parseScript('let component = '.concat('{', script, '}')).body[0].declarations[0].init.properties.forEach(script_prop => {
//         script_part_list.push({name: script_prop.key.name , data: lib_escodegen.generate(script_prop.value)})
//     })

//     let component_name = ''
//     let component_script = []

//     script_part_list.forEach(part => {
//         if (part.name === 'name') {
//             component_name = lib_conv.border_del(part.data, "'", "'")
//         } else {
//             component_script.push(part.name.concat(" :", part.data))
//         }
//     })
//     if (lib_conv.isEmpty(component_name)) {
//         component_name = lib_conv.toString(name,'')
//     }
//     if (lib_conv.isEmpty(component_name)) {
//         component_name = 'undefined_name'
//     }

//     let js_script = [
//         lib_conv.format('Vue.component("{0}", {', component_name),
//         "template: '",
//         template,
//         "',",
//         component_script.join(','.concat(lib_os.EOL)),
//         "})"
//     ].join(lib_os.EOL)

//     return js_script
// }
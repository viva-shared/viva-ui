//@ts-check

const lib_os = require('os')
const lib_conv = require('viva-convert')
const lib_strip = require('strip-comments')
const lib_esprima = require('esprima')
const lib_escodegen = require('escodegen')
const lib_compile = require('./compile.js')

exports.go = go

/**
 * @param {string} data
 * @param {string} component_name
 * @returns {lib_compile.type_to_store}
 */
function go(data, component_name) {
    return {
        data_base64: lib_compile.file_data_beautify(convert(data, component_name)),
        content_type: 'text/html; charset=UTF-8'
    }
}

/**
 * @property {string} vue
 * @property {string} name
 * @returns {string}
 */
function convert(vue, name) {
    let v = lib_conv.toString(vue, '').replace(/<\!--.*?-->/g, "")

    let find_template_start = v.indexOf('<template>')
    if (find_template_start < 0) {
        throw Error('in component not find "find_template_start"')
    }
    let find_template_stop = v.indexOf('</template>', find_template_start)
    if (find_template_stop < 0) {
        throw Error('in component not find "find_template_stop"')
    }
    let template = v.substring(find_template_start, find_template_stop + '</template>'.length)

    template = lib_conv.replaceAll(template, lib_os.EOL, ' ')
    template = lib_conv.replaceAll(template, '\n', ' ')

    let find_script_start = v.indexOf('<script>')
    if (find_script_start < 0) {
        throw Error('in component not find "find_script_start"')
    }
    let find_script_stop = v.indexOf('</script>', find_script_start)
    if (find_script_stop < 0) {
        throw Error('in component not find "find_script_stop"')
    }
    let script = lib_strip(v.substring(find_script_start + '<script>'.length, find_script_stop), undefined).trim()

    if (script.substring(0, 'export'.length) !== 'export') {
        throw Error('in component not find "export default {" in start script')
    } else {
        script = script.substring('export'.length, script.length).trim()
    }
    if (script.substring(0, 'default'.length) !== 'default') {
        throw Error('in component not find "export default {" in start script')
    } else {
        script = script.substring('default'.length, script.length).trim()
    }
    if (script.substring(0, '{'.length) !== '{') {
        throw Error('in component not find "export default {" in start script')
    } else {
        script = script.substring('{'.length, script.length).trim()
    }
    if (script.substring(script.length - '}'.length, script.length) !== '}') {
        throw Error('in component not find "}" in end script')
    } else {
        script = script.substring(0, script.length - '}'.length).trim()
    }

    let script_part_list = []

    // @ts-ignore
    lib_esprima.parseScript('let component = '.concat('{', script, '}')).body[0].declarations[0].init.properties.forEach(script_prop => {
        script_part_list.push({name: script_prop.key.name , data: lib_escodegen.generate(script_prop.value)})
    })

    let component_name = ''
    let component_script = []

    script_part_list.forEach(part => {
        if (part.name === 'name') {
            component_name = lib_conv.border_del(part.data, "'", "'")
        } else {
            component_script.push(part.name.concat(" :", part.data))
        }
    })
    if (lib_conv.isEmpty(component_name)) {
        component_name = lib_conv.toString(name,'')
    }
    if (lib_conv.isEmpty(component_name)) {
        component_name = 'undefined_name'
    }

    let js_script = [
        lib_conv.format('Vue.component("{0}", {', component_name),
        "template: '".concat(template, "',"),
        component_script.join(','.concat(lib_os.EOL)),
        "})"
    ].join(lib_os.EOL)

    return js_script
}
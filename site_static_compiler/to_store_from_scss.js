//@ts-check

const lib_os = require('os')
const lib_conv = require('viva-convert')
const lib_compile = require('./compile.js')

exports.go = go

/**
 * @param {string} data
 * @returns {lib_compile.type_to_store}
 */
function go(data) {
    return {
        data_base64: lib_compile.file_data_beautify(data),
        content_type: 'text/css; charset=UTF-8'
    }
}
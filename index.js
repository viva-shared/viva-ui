//@ts-check
const lib_fs = require('fs-extra')
const lib_path = require('path')
const lib_conv = require('viva-convert')
const lib_compile = require('./site_static_compiler/compile.js')


exports.compile = compile

/**
 * @param {string} site_source_path folder in your project, wich contains source (e.g. *.vue) for site, example - require('path').join(__dirname, 'site_source')
 * @param {string} site_compiled_path folter in your project, where to save compiled file, example - require('path').join(__dirname, 'site_compiled')
 * @param {string} root_vue_file_path path to root your vue-file in site, example - 'vue/root.vue'
 * @param {string} root_vue_name name your root vue component in site
 */
function compile(site_source_path, site_compiled_path, root_vue_file_path, root_vue_name) {
    lib_fs.emptyDirSync(site_compiled_path)
    let internal_site_source_path = lib_path.join(__dirname, 'site_source')
    lib_fs.writeFileSync(lib_path.join(site_compiled_path, 'store_internal.js'), lib_compile.go(internal_site_source_path, root_vue_file_path, root_vue_name), 'utf8')
    lib_fs.writeFileSync(lib_path.join(site_compiled_path, 'store_external.js'), lib_compile.go(site_source_path, root_vue_file_path, root_vue_name), 'utf8')
}

// //let store = lib_compile.go()
// let site_path = lib_path.join(__dirname, 'site_compiled')
// lib_fs.emptyDirSync(site_path)
// lib_fs.writeFileSync(lib_path.join(site_path, 'store.js'), store, {encoding: 'utf8'})






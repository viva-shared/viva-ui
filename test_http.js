//@ts-check

const lib_conv = require('viva-convert')
const lib_http = require('viva-server-http')
const store = require('./site_compiled/store.js')

let http = new lib_http()

http.on('listening', function(host) {
    console.log ('http server ' + host + ' started...')
})

http.on('post', function(data, sender, incomingMessageHttp, serverResponseHttp) {
    sender(200, data)
})

http.on('get', function(data, sender, incomingMessageHttp, serverResponseHttp) {
    let html = store.get(incomingMessageHttp.url)
    if (lib_conv.isAbsent(html)) {
        sender(404, lib_conv.format("unknown path {0}", incomingMessageHttp.url))
        return
    }
    serverResponseHttp.setHeader('Content-Type', html.content_type)
    sender(200, html.data)
})

http.on('error', function(error, sender, incomingMessageHttp, serverResponseHttp) {
    sender(500,error.toString())
})

try {
    http.start('127.0.0.1',3042)
} catch (error) {
    console.log(error)
}